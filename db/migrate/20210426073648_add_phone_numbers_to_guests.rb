class AddPhoneNumbersToGuests < ActiveRecord::Migration[5.2]
  def change
    add_column :guests, :phone_numbers, :string, array: true, null: false, default: []
  end
end
