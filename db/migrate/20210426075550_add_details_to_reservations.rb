class AddDetailsToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :currency, :string
    add_column :reservations, :payout_amount, :decimal, precision: 10, scale: 2
    add_column :reservations, :total_amount, :decimal, precision: 10, scale: 2
    add_column :reservations, :security_amount, :decimal, precision: 10, scale: 2
    add_column :reservations, :guest_details, :jsonb, null: false, default: {}
  end
end
