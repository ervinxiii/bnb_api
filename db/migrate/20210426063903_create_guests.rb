class CreateGuests < ActiveRecord::Migration[5.2]
  def change
    create_table :guests do |t|
      t.string :email, limit: 60, default: "", null: false
      t.string :first_name
      t.string :last_name

      t.timestamps
    end
  end
end
