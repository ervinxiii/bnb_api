module Api
  module V1
    class GuestsController < ApplicationController
      def index
        guests = Guest.order(created_at: :desc)

        render json: { status: "Success", message: "Guest data loaded", data: guests }, status: :ok
      end
    end
  end
end
