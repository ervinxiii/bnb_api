module Api
  module V1
    class ReservationsController < ApplicationController
      def index
        reservations = Reservation.order(created_at: :desc)

        render json: { status: "Success", message: "Reservation loaded", data: reservations }, status: :ok
      end

      def create
        guest = Guest.find_by_email(guest_email)
        guest_parameter = JSON.parse(guest_params.to_json) if guest.nil?
        reservation_parameter = JSON.parse(reservation_params.to_json)

        processor = ReservationProcessor.new(reservation_parameter, guest_parameter, guest_id: guest.try(:[], :id))

        if processor.execute
          render json: {
            status: "success",
            message: "Reservation was saved",
          }, status: :ok
        else
          render json: {
            status: "error",
            message: "Error occured while saving reservation"
          }, status: :unprocessable_entity
        end
      end

      private

      def guest_email
        params[:guest].try(:[], :email) || params[:reservation].try(:[], :guest_email)
      end

      def guest_params
        if params.keys.count > 3
          params[:guest].permit(:email, :first_name, :last_name, :phone)
        else
          params[:reservation].permit(:guest_email, :guest_first_name, :guest_id, :guest_last_name, guest_phone_numbers: [])
        end
      end

      def reservation_params
        if params.keys.count > 3
          params.permit(:start_date, :end_date, :nights, :status, :currency, :payout_price, :security_price, :total_price, :nights, :guests, :adults, :children, :infants)
        elsif params[:reservation].present?
          params[:reservation].permit(:start_date, :end_date, :nights, :adults, :children, :infants, :status_type, :listing_security_price_accurate, :host_currency, :nights, :number_of_guests, :status_type, :total_paid_amount_accurate, guest_details: {})
        end
      end
    end
  end
end
