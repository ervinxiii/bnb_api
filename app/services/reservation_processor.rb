class ReservationProcessorError < StandardError; end

class ReservationProcessor
  attr_reader :reservation_params, :guest_params
  def initialize(reservation_params, guest_params, options={})
    @reservation_params = reservation_params
    @guest_params = guest_params
    @guest_id = options[:guest_id]
  end

  def execute
    if @guest_id.present?
      begin
        reservation = Reservation.new(build_reservation.merge(guest_id: @guest_id))

        if reservation.save
          true
        else
          false
        end
      rescue StandardError => e
        false
      end
    else
      begin
        guest = Guest.new(build_guest)
        if guest.save
          reservation = Reservation.new(build_reservation.merge(guest_id: guest.id))
          if reservation.save
            true
          else
            false
          end
        end
      rescue StandardError => e
        false
      end
    end
  end

  private

  def build_guest
    {
      email: guest_params["guest_email"] || guest_params["email"],
      first_name: guest_params["guest_first_name"] || guest_params["first_name"],
      last_name: guest_params["guest_last_name"] || guest_params["last_name"],
      phone_numbers: guest_params["guest_phone_numbers"] || [guest_params["phone"]]
    }
  end

  def build_reservation
    {
      start_date: DateTime.parse(reservation_params["start_date"]), 
      end_date: DateTime.parse(reservation_params["end_date"]),
      number_of_nights: reservation_params["nights"],
      status: reservation_params["status"] || reservation_params["status_type"],
      currency: reservation_params["currency"] || reservation_params["host_currency"], 
      payout_amount: reservation_params["payout_price"] || reservation_params["expected_payout_amount"],
      total_amount: reservation_params["total_paid_amount_accurate"] || reservation_params["total_price"],
      security_amount: reservation_params["listing_security_price_accurate"] || reservation_params["security_price"],
      guest_details: build_guest_details
    }
  end

  def build_guest_details
    {
      number_of_guests: reservation_params["number_of_guests"] || reservation_params["guests"],
      localized_description: reservation_params["guest_details"].try(:[], "localized_description"),
      number_of_adults: reservation_params["adults"] || reservation_params["guest_details"]["number_of_adults"],
      number_of_children: reservation_params["children"] || reservation_params["guest_details"]["number_of_children"],
      number_of_infants: reservation_params["infants"] || reservation_params["guest_details"]["number_of_infants"]
    }
  end
end
