class Reservation < ApplicationRecord
  belongs_to :guest

  validates :start_date, :end_date, :number_of_nights, presence: true
end
