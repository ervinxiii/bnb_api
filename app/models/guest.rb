class Guest < ApplicationRecord
  has_many :reservations

  validates :email, presence: true, uniqueness:true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :first_name, :last_name, presence: true
end
