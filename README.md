## BNB API

### Version

```
  Ruby 2.5.7
  Rails 5.2.5
  Postgres 10.5
```

### Installation
```
  $ git clone git@gitlab.com:ervinxiii/bnb_api.git
  or
  $ git clone https://gitlab.com/ervinxiii/bnb_api.git
  $ cd bnb_api
```

### Setup

```
  $ bundle install
```

### Database Creation

```
  $ rake db:create db:migrate db:test:prepare
```

### Running the server

```
  $ bundle exec rails server
```

### How to send payload

After starting the server, use an API tester tool (e.g. Postman, Insomnia)

1. Create a new POST request and set the request URL to `http://localhost:3000/api/v1/reservations/`
2. Set the header Content-Type application/json
3. Insert the request payload as JSON
4. Send the request

### Check created reservations and guests

For reservations:
1. Create a new GET request and set the request URL to `http://localhost:3000/api/v1/reservations/`
2. Send the request

For guests:
1. Create a new GET request and set the request URL to `http://localhost:3000/api/v1/guests/`
2. Send the request
