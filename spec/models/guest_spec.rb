require 'rails_helper'

RSpec.describe Guest, type: :model do
  subject {
    described_class.new(email: "test@hello.com", first_name: "test", last_name: "user")
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is invalid without email" do
    subject.email = nil
    expect(subject).to_not be_valid
  end

  it "is invalid with invalid email" do
    subject.email = 'test'
    expect(subject).to_not be_valid
  end

  it "is invalid without first_name" do
    subject.first_name = nil
    expect(subject).to_not be_valid
  end

  it "is invalid without last_name" do
    subject.last_name = nil
    expect(subject).to_not be_valid
  end
end
